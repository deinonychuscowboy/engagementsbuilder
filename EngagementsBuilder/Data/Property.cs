﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace EngagementsBuilder.Data
{
	/// <summary>
    /// Operation that is being applied to the ship in the stock ship file
    /// </summary>
	public interface Operation { }

    /// <summary>
    /// Delegate for transforming a given value in the ship
    /// </summary>
	public delegate string TransformValue(string ship,string input);

    /// <summary>
	/// Operation which uses function call type format like foo(bar,baz)
    /// </summary>
	public struct FuncCall:Operation
	{
		public string Key { get; private set; }
		public string[] Args { get; private set; }
		public string Type { get; private set; }

		public FuncCall(string key,string type,string[] args)
			: this()
		{
			this.Key=key;
			this.Args=args;
			this.Type = type;
		}
	}

    /// <summary>
    /// Operation which uses an assignment type format like foo.bar=baz
    /// </summary>
	public struct Assignment : Operation
	{
		public string Key { get; private set; }
		public string Type { get; private set; }
		public string Arg { get; private set; }

		public Assignment(string key,string type,string arg)
			: this()
		{
			this.Key=key;
			this.Type = type;
			this.Arg=arg;
		}
	}

    /// <summary>
    /// Operation for any other line
    /// </summary>
	public struct Literal:Operation
	{
		public string Text{get;private set;}

		public Literal(string text):this()
		{
			this.Text=text;
		}
	}

    /// <summary>
    /// Identifiable value that needs to be transformed by EngagementsBuilder and mapped to a property in the props folder
    /// </summary>
	public abstract class Property
	{
		/// <summary>
        /// Name of the property. This is the most clearly identifiable distinct designation for the value, e.g. the specific argument in a function call that needs to be replaced
        /// </summary>
		public string Name { get; private set; }
        /// <summary>
        /// Key of the property. This is the highest-level designation for the entire operation line, e.g. the main function being called
        /// </summary>
		public string Key { get; private set; }
        /// <summary>
        /// Type of the property. This can be Ship or SubSystem, depending on what you're configuring.
        /// </summary>
		public string Type { get; private set; }
        /// <summary>
        /// Value of the property. This may be a plain value or a mapping to a property
        /// </summary>
		public string Value { get; protected set; }
        /// <summary>
        /// Original value of the property or the specified default in the property mapping
        /// </summary>
		public string Default { get; protected set; }
        /// <summary>
		/// Line number in the original ship file. May not be correct (shhhh) but useful in some cases
        /// </summary>
		public int Line { get; protected set; }
        /// <summary>
        /// Transformer that takes in the original value and returns the final value.
        /// </summary>
		private TransformValue transformer;
        /// <summary>
        /// Regex to detect whether the value is already mapped to the props folder or not -- this affects what the "real value" is
        /// </summary>
		protected Regex presub;

		public Property(string name,string key,string type,TransformValue transformer)
		{
			this.Name=name;
			this.Key=key;
			this.Type=type;
			this.transformer=transformer;
			this.presub=new Regex(@"get"+this.Type+@"Num\( ?New"+this.Type+@"Type, ?""[^""]*"", ?([^\)]*) ?\)");
		}

        /// <summary>
		/// This creates the actual prop lookup for the ship file (internal formatting method)
        /// </summary>
		protected string subValue()
		{
			return "get" + this.Type + "Num(New"+this.Type+"Type, \""+this.Name+"\", "+this.Default+")";
		}

        /// <summary>
		/// This creates the actual prop value for the props file (what the above call looks up)
        /// </summary>
		public Assignment Map(string ship)
		{
			return new Assignment(this.Name,null,this.transformer(ship,this.Value));
		}

        /// <summary>
		/// This creates the actual prop lookup for the ship file
        /// </summary>
        /// <returns>The sub.</returns>
		public abstract Operation Sub();

        /// <summary>
        /// Applies the property rule to a specific operation from the ship file. This is where the abstract concept of a property becomes a concrete replacement in relation to a specific operation in the ship file.
        /// </summary>
		public abstract void Load(Operation[] data);
	}

    /// <summary>
	/// Implementation of a property for an object-based rule type (Assignment that begins with NewShipType.stuff=)
    /// </summary>
	public class ObjProperty:Property
	{
		public ObjProperty(string name,string key,string type,TransformValue transformer) : base(name,key,type,transformer) { }

        /// <inheritdoc/>
		public override void Load(Operation[] operations)
		{
			List<Assignment> list=new List<Assignment>();
			for(int index=0;index<operations.Length;index++) {
				if(operations[index] is Assignment) {
					Assignment assg=(Assignment)operations[index];
					if(this.Key==assg.Key&& this.Type == assg.Type) {
						list.Add(assg);
						this.Line=index;
					}
				}
			}
			Assignment[] assgs=list.ToArray();

			if(assgs.Length==0) {
				throw new ApplicationException("No valid assignment");
			}
			if(assgs.Length>1) {
				throw new InvalidOperationException("Invalid data");
			}
			this.Value=assgs[0].Arg;

			if(this.presub.IsMatch(this.Value)) {
				this.Value=this.presub.Match(this.Value).Groups[1].Value;
			}

			this.Default=this.Value;
		}

        /// <inheritdoc/>
		public override Operation Sub()
		{
			return new Assignment(this.Key,this.Type,this.subValue());
		}
	}
    
    /// <summary>
    /// Implementation of a property for a function-based rule type
    /// </summary>
	public class FuncProperty:Property
	{
		private string[] origArgs;
		private Regex namedArg;
		private string namedArgName;
		public Regex[] Args { get; private set; }
		public int Index { get; private set; }

        /// <summary>
        /// Aside from the basic populating args that are similar to ObjProperty, functions also need a set of regexes to match their arguments, and an index of which arg this property is modifying
        /// </summary>
		public FuncProperty(string name,string key,string type,TransformValue transformer,Regex[] args,int index)
			: base(name,key,type,transformer)
		{
			this.Args=args;
			this.namedArg=new Regex(@"^\{ ?([^=]*) ?= ?""([^""]*)"" ?\}$");
			this.Index=index;
		}

        /// <inheritdoc/>
		public override void Load(Operation[] operations)
		{
			List<FuncCall> list=new List<FuncCall>();
			for(int index=0;index<operations.Length;index++) {
				if(operations[index] is FuncCall) {
					FuncCall call=(FuncCall)operations[index];
					if(this.Key==call.Key&&this.Type==call.Type&&this.argMatch(call)) {
						list.Add(call);
						this.Line=index;
					}
				}
			}
			FuncCall[] calls=list.ToArray();

			if(calls.Length==0) {
				throw new ApplicationException("No valid funccall");
			}
			if(calls.Length>1) {
				throw new InvalidOperationException("Invalid data");
			}
			this.origArgs=calls[0].Args;
			this.Value=calls[0].Args[this.Index];
			if(this.namedArg.IsMatch(this.Value)) {
				Match m=this.namedArg.Match(this.Value);
				this.namedArgName=m.Groups[1].Value;
				this.Value=m.Groups[2].Value;
			}

			if(this.presub.IsMatch(this.Value)) {
				this.Value=this.presub.Match(this.Value).Groups[1].Value;
			}

			this.Default=this.Value;
		}

        /// <summary>
        /// Special method for determining if a real operation from the ship file matches this property by comparing the arguments to each to be sure they are the same
        /// </summary>
		private bool argMatch(FuncCall call)
		{
			if(this.Args.Length>call.Args.Length) {
				return false;
			}
			for(int i=0;i<this.Args.Length;i++) {
				if(!this.Args[i].IsMatch(call.Args[i])) {
					return false;
				}
			}
			return true;
		}

        /// <inheritdoc/>
		public override Operation Sub()
		{
			string[] newargs=this.origArgs;
			newargs[this.Index]=this.subValue();
			if(this.namedArgName!=null) {
				newargs[this.Index]="{"+this.namedArgName+"="+newargs[this.Index]+"}";
			}
			return new FuncCall(this.Key,this.Type,newargs);
		}
	}

}
