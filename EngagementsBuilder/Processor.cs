﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using EngagementsBuilder.Data;

namespace EngagementsBuilder
{
	/// <summary>
    /// Processes given property rules for a specific homeworld ship
    /// </summary>
	public class Processor
	{
		private Compiler compiler;
		private string inputPath;
		private string outputPath;
		private string projname;
		private bool single;

		private Property[] properties;

		public Processor(string projname,string inputPath,string outputPath, bool single,Property[] properties)
		{
			this.compiler=new Compiler();
			this.inputPath=inputPath;
			this.outputPath=outputPath;
			this.projname=projname;
			this.properties=properties;
			this.single = single;
		}

        /// <summary>
		/// Breaks apart files semi-intelligently by lines, e.g. if you end a line with ( in the ship file the next line in the file will be considered a continuation of the previous line
        /// </summary>
        /// <returns>The parse.</returns>
        /// <param name="input">Input.</param>
		private string[] parse(string input)
		{
			int i=0;
			string cur="";
			int comment=0;
			List<string> set=new List<string>();
			foreach(char c in input) {
				if(comment==2&&c!='\n') {
					continue;
				}
				switch(c) {
					case '(':
						comment=0;
						i++;
						cur+=c;
						break;
					case ')':
						comment=0;
						i--;
						cur+=c;
						break;
					case '\r':
						comment=0;
						break;
					case '\n':
						comment=0;
						if(i==0) {
							if(cur!="") {
								set.Add(cur.Trim());
							}
							cur="";
						}
						break;
					case '-':
						comment++;
						break;
					default:
						comment=0;
						cur+=c;
						break;
				}
			}
			if(cur!="") {
				set.Add(cur.Trim());
			}
			set.Add("");

			return set.ToArray();
		}

        /// <summary>
        /// Process a given ship
        /// </summary>
		public void Process(string shipname)
		{
			string path=this.inputPath+Path.DirectorySeparatorChar+shipname+Path.DirectorySeparatorChar+shipname+".ship";
			if(!File.Exists(path)) {
				Debug.Write(shipname+" not found");
				return;
			}
			using(StreamReader file=new StreamReader(File.OpenRead(path))) {
				// we do a couple gross replacements here since the vanilla ship files have some bugs and variations
				string inputShip=file.ReadToEnd().Replace("NewShipTypeuseEngagementRanges=1","NewShipType.useEngagementRanges=1").Replace("NewSubSystemTypeuseEngagementRanges=1", "NewSubSystemType.useEngagementRanges=1").Replace("newShipType", "NewShipType").Replace("newSubSystemType", "NewSubSystemType");
				string[] input=this.parse(inputShip);
				Operation[] operations=(from string line in input select this.compiler.Decompile(line)).ToArray();
				List<Property> validProperties=new List<Property>(this.properties);
				List<Property> invalid=new List<Property>();

                // determine what needs to be done for this specific ship
				foreach(Property property in validProperties) {
					try {
						property.Load(operations);
					} catch(ApplicationException e) {
                        // this is often okay, e.g. resource collectors are the only ships that will have a resource capacity line and everything else will throw an error on that property
						if(e.Message=="No valid funccall") {
							//Console.WriteLine("Unable to find funccall for "+property.Name+" in "+shipname);
						}
						if(e.Message=="No valid assignment") {
							//Console.WriteLine("Unable to find assignment for "+property.Name+" in "+shipname);
						}
						invalid.Add(property);
					}
				}

				foreach(Property invalidProperty in invalid) {
					validProperties.Remove(invalidProperty);
				}

                // determine what lines need to be changed for the properties that are valid for this ship
				int[] subbedLines=(from Property property in validProperties select property.Line).ToArray();

				string[] output=new string[operations.Length];
				List<string> mappings=new List<string>();

                // do the transformation and subbing out
				for(int i=0;i<operations.Length;i++) {
					if(subbedLines.Contains(i)) {
						Property[] props=(from Property prop in validProperties where prop.Line==i select prop).ToArray();

                        // if there is more than one property, we need to be a little more intelligent about how our changes will affect later properties
                        // i.e. two properties might need to modify the same line, so we need to make sure the second one is able to make its changes too after the first one finishes
						Operation op=operations[i];

						foreach(Property property in props) {
							// Line will not be correct here, but it doesn't matter since we're already compiling and don't need to look at it again
							property.Load(new Operation[]{op});
							output[i]=this.compiler.Compile(property.Sub());
							mappings.Add(this.compiler.Compile(property.Map(shipname)));
							op=this.compiler.Decompile(output[i]);
						}
					} else {
						output[i]=input[i];// don't recompile unchanged lines
					}
				}
				Console.WriteLine("\t"+mappings.Count+" transformations executed");

                // now we write out the changed files
				string outputShip=String.Join("\r\n",output);
				string outputMappings=@"
Number_Properties_Priority = 10.0

Number_Properties = {
"+String.Join(",\r\n",mappings.ToArray())+@",
}

String_Properties_Priority = 10.0

String_Properties = {
}
";

				Directory.CreateDirectory(this.outputPath+Path.DirectorySeparatorChar+shipname+Path.DirectorySeparatorChar+"deathmatch"+Path.DirectorySeparatorChar+"props");
                Directory.CreateDirectory(this.outputPath + Path.DirectorySeparatorChar + shipname + Path.DirectorySeparatorChar + "singleplayer" + Path.DirectorySeparatorChar + "props");
				string shippath = this.outputPath + Path.DirectorySeparatorChar + shipname + Path.DirectorySeparatorChar + shipname + ".ship";
                string mappath = this.outputPath + Path.DirectorySeparatorChar + shipname + Path.DirectorySeparatorChar + "deathmatch" + Path.DirectorySeparatorChar + "props" + Path.DirectorySeparatorChar + this.projname + ".lua";
				string spmappath = this.outputPath + Path.DirectorySeparatorChar + shipname + Path.DirectorySeparatorChar + "singleplayer" + Path.DirectorySeparatorChar + "props" + Path.DirectorySeparatorChar + this.projname + ".lua";
				using(StreamWriter shipout=new StreamWriter(File.Open(shippath,FileMode.Create))) {
					shipout.Write(outputShip);
					Console.WriteLine("\t" + shippath);
				}
				using(StreamWriter mapout=new StreamWriter(File.Open(mappath,FileMode.Create))) {
					mapout.Write(outputMappings);
					Console.WriteLine("\t" + mappath);
				}
				if (this.single)
				{
					using (StreamWriter spmapout = new StreamWriter(File.Open(spmappath, FileMode.Create)))
					{
						spmapout.Write(outputMappings);
						Console.WriteLine("\t" + spmappath);
					}
				}
			}
		}
	}
}