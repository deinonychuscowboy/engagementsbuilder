﻿using System;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using EngagementsBuilder.Data;

namespace EngagementsBuilder
{
	/// <summary>
    /// This main class determines what rules the builder will apply to all homeworld ships.
	/// 
	/// If you want to use EngagementsBuilder to make your own mod (you poor sucker), this should be the only place you need to change stuff.
    /// </summary>
	public class Program
	{
		public static void Main(string[] args)
		{
			// ugh this may be the grossest CLI I've ever made
			if(args.Length!=4&&args.Length!=3){
				Console.WriteLine("USAGE: EngagementsBuilder.exe input output scalefactor [singleplayer]");
				Console.WriteLine("\tinput:        /path/to/extracted/big/file/");
				Console.WriteLine("\toutput:       /path/to/output/directory/");
				Console.WriteLine("\tscalefactor:  10 (base value for health increase)");
				Console.WriteLine("\tsingleplayer: optional, 'sp' to include singleplayer, nothing to only use multiplayer");
			}

            // self-explanantory
			bool singleplayer = args.Length>3&&args[3] == "sp";

            // engagements is all about increasing the health of ships and other related factors, so the base health scaling factor is so important we give it on the command line and use it in everything below here
			double scalefactor = double.Parse(args[2]);

            // some regular expressions to determine when a given transformation should be applied on a ship-to-ship basis
            // e.g. this first one is used later to modify the max speed of only salvage corvettes
			Regex salcapcorv = new Regex(@"(?:kus|tai)_salvagecorvette");
			Regex cruiser = new Regex(@"(?:kus|tai|hgn|vgr)_(?:heavycruiser|battlecruiser)");
			Regex supercap = new Regex(@"(?:kus|tai|hgn|vgr)_(?:carrier|heavycruiser|battlecruiser|shipyard|mothership)");
			Regex cap = new Regex(@"(?:kus|tai|hgn|vgr)_(?:destroyer|missiledestroyer)");
			Regex resourcecollectorrepair = new Regex(@"(?:hgn|vgr)_resourcecollector");
			Regex repaircorvette = new Regex(@"(?:kus|tai)_repaircorvette");
			Regex supportfrigate = new Regex(@"(?:kus|tai)_supportfrigate");
			Regex origassaultfrigate = new Regex(@"(?:kus|tai)_assaultfrigate");

			// this is the big list of rules about what to change, <see cref="Data/Property.cs"/> for more detail on what all these arguments do
			Processor proc = new Processor("engagements", args[0], args[1], singleplayer,
				new Property[] {
					// health and regen
    			    new ObjProperty("maxhealth","maxhealth","Ship",(ship,input)=>input+"*"+scalefactor),
    				new ObjProperty("regentime","regentime","Ship",(ship,input)=>"0"),
    				new ObjProperty("minRegenTime","minRegenTime","Ship",(ship,input)=>"0"),

					// dock repair speed (complicated because different ships have the arguments in different places)
    				new FuncProperty("dockRepairDefault","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold""")},6),

    				new FuncProperty("dockRepairSuperCap","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"SuperCap")},7),
    				new FuncProperty("dockRepairSuperCap","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"SuperCap")},8),
    				new FuncProperty("dockRepairSuperCap","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"SuperCap")},9),
    				new FuncProperty("dockRepairSuperCap","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"SuperCap")},10),

    				new FuncProperty("dockRepairFrigate","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Frigate")},7),
    				new FuncProperty("dockRepairFrigate","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Frigate")},8),
    				new FuncProperty("dockRepairFrigate","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Frigate")},9),
    				new FuncProperty("dockRepairFrigate","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Frigate")},10),

    				new FuncProperty("dockRepairFighter","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Fighter")},7),
    				new FuncProperty("dockRepairFighter","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Fighter")},8),
    				new FuncProperty("dockRepairFighter","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Fighter")},9),
    				new FuncProperty("dockRepairFighter","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Fighter")},10),

    				new FuncProperty("dockRepairCorvette","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Corvette")},7),
    				new FuncProperty("dockRepairCorvette","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Corvette")},8),
    				new FuncProperty("dockRepairCorvette","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Corvette")},9),
    				new FuncProperty("dockRepairCorvette","addAbility","Ship",(ship,input)=>input+"*"+(scalefactor/2d),new Regex[]{new Regex(@"""ShipHold"""),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@".*"),new Regex(@"Corvette")},10),

					// repair ships (repair corvettes, support frigates, resource collectors)
    				new FuncProperty("repairRate","addAbility","Ship",(ship,input)=>
    					{
    						if(supportfrigate.IsMatch(ship)){
    							return input+"*"+(scalefactor/10d*3);
    						}else if(resourcecollectorrepair.IsMatch(ship)){
    							return input+"*"+(scalefactor/10d*2.5);
    						}else if(repaircorvette.IsMatch(ship)) {
    							return input+"*"+(scalefactor/5d);
    						} else {
    							return input;
    						}
    					},new Regex[]{new Regex(@"""RepairCommand""")},3),

					// marine frigate capture time
    				new FuncProperty("captureTime","addAbility","Ship",(ship,input)=>
    					{
    						if(supercap.IsMatch(ship)){
    							return input+"*"+(scalefactor*2);
    						}else if(cap.IsMatch(ship)){
    							return input+"*"+(scalefactor*1.5);
    						}else {
    							return input+"*"+scalefactor;
    						}
    					},new Regex[]{new Regex(@"""CanBeCaptured""")},1),

					// resource collector harvest capacity
    				new FuncProperty("harvestCapacity","addAbility","Ship",(ship,input)=>
    					{
    						return input+"*"+(scalefactor/5d);
    					},new Regex[]{new Regex(@"""Harvest""")},2),

					// resource collector harvest rate
    				new FuncProperty("harvestRate","addAbility","Ship",(ship,input)=>
    					{
					return input+"/"+string.Format("{0:F1}",scalefactor/5d);
    					},new Regex[]{new Regex(@"""Harvest""")},3),

					// engine speed (for salvage corvettes)
    				new ObjProperty("thrusterMaxSpeed","thrusterMaxSpeed","Ship",(ship,input)=>
    					{
    						if(salcapcorv.IsMatch(ship)) {
						return input+"*"+(scalefactor/10d*4)+"/"+string.Format("{0:F1}",scalefactor/2d);
    						} else {
    							return input;
    						}
    					}),
					
					// engine speed (for salvage corvettes) -- don't ask me what the difference between thrusterMaxSpeed and mainEngineMaxSpeed is
    				new ObjProperty("mainEngineMaxSpeed","mainEngineMaxSpeed","Ship",(ship,input)=>
    					{
    						if(salcapcorv.IsMatch(ship)) {
						return input+"*"+(scalefactor/10d*4)+"/"+string.Format("{0:F1}",scalefactor/2d);
    						} else {
    							return input;
    						}
    					}),

					// build time (for capships)
    				new ObjProperty("buildTime","buildTime","Ship",(ship,input)=>
    					{
    						if(cruiser.IsMatch(ship)) {
    							return input+"*"+(scalefactor/10d*2.75);
    						} else if(supercap.IsMatch(ship)) {
    							return input+"*"+(scalefactor/5d);
    						}else if(cap.IsMatch(ship)){
    							return input+"*"+(scalefactor/10d*1.25);
    						}else {
    							return input;
    						}
    					}),

					// class bonuses
    				new ObjProperty("antiFighterValue","antiFighterValue","Ship",(ship,input)=>
    					{
    						return input+"*"+(scalefactor/10d*4);
    					}),
    				new ObjProperty("antiCorvetteValue","antiCorvetteValue","Ship",(ship,input)=>
    					{
    						if(origassaultfrigate.IsMatch((ship))) {
    							return "12*"+(scalefactor/10d*3); // (default is 0, default fighter value is 12)
    						} else {
    							return input+"*"+(scalefactor/10d*3);
    						}
    					}),
				}
			);

			DirectoryInfo dirroot = new DirectoryInfo(args[0]);
			foreach (DirectoryInfo dir in dirroot.GetDirectories())
			{
				Console.WriteLine(dir.Name);
				proc.Process(dir.Name);
			}
		}
	}
}
