﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EngagementsBuilder.Data;

namespace EngagementsBuilder
{
	/// <summary>
    /// This is a minimal regex-based compiler/decompiler for the subset of lua used in homeworld's ship files. It should be able to parse and represent everything in a ship file as internal data structures.
    /// </summary>
	public class Compiler
	{
		private Regex assignments;
		private Regex funccalls;
		private Regex conditional;

		public Compiler()
		{
			this.assignments=new Regex(@"^New((?:Ship)|(?:SubSystem))Type\.([^ =]*) ?= ?(.*);?$");
			this.funccalls=new Regex(@"^([^\(]*) ?\( ?[Nn]ew((?:Ship)|(?:SubSystem))Type, ?(.*)\);?$");
			this.conditional=new Regex(@"^if .* then$");
		}

        /// <summary>
        /// Internal method to handle the arguments given to a function call in the ship file
        /// </summary>
		private string[] parseArgs(string input)
		{
			int i=0;
			bool str=false;
			string cur="";
			int comment=0;
			List<string> set=new List<string>();
			foreach(char c in input) {
				if(comment==2) {
					continue;
				}
				switch(c) {
					case '"':
						comment=0;
						str=!str;
						cur+=c;
						break;
					case '(':
						comment=0;
						i++;
						cur+=c;
						break;
					case ')':
						comment=0;
						i--;
						cur+=c;
						break;
					case '\r':
						comment=0;
						break;
					case ',':
						comment=0;
						if(!str&&i==0) {
							if(cur!="") {
								set.Add(cur.Trim());
							}
							cur="";
						} else {
							cur+=c;
						}
						break;
					case '-':
						comment++;
						break;
					default:
						comment=0;
						cur+=c;
						break;
				}
			}
			if(cur!="") {
				set.Add(cur.Trim());
			}
            
			return set.ToArray();
		}

        /// <summary>
        /// Compiles internal data structures into lua text for putting into homeworld files
        /// </summary>
		public string Compile(Operation operation)
		{
			if(operation is Assignment) {
				Assignment assg=(Assignment)operation;
				return (assg.Type!=null?"New"+assg.Type+"Type.":"")+assg.Key+"="+assg.Arg;
			} else if(operation is FuncCall) {
				FuncCall call=(FuncCall)operation;
				return call.Key+"(New"+call.Type+"Type, "+String.Join(", ",call.Args)+")";
			} else if(operation is Literal) {
				return ((Literal)operation).Text;
			}

			throw new ArgumentException("Unknown statement type");
		}

        /// <summary>
        /// Decompiles lua text from homeworld files into internal data structures
        /// </summary>
		public Operation Decompile(string smt)
		{
			if(this.assignments.IsMatch(smt)) {
				Match match=this.assignments.Match(smt);
				return new Assignment(match.Groups[2].Value.Trim(),match.Groups[1].Value.Trim(), match.Groups[3].Value.Trim());
			}
			if(this.funccalls.IsMatch(smt)) {
				Match match=this.funccalls.Match(smt);
				string[] args=this.parseArgs(match.Groups[3].Value);
				for(int i=0;i<args.Length;i++) {
					args[i]=args[i].Trim();
				}
				return new FuncCall(match.Groups[1].Value.Trim(),match.Groups[2].Value.Trim(),args);
			} else if(this.conditional.IsMatch(smt)||smt=="end"){
				return new Literal(smt);
			}else if(smt=="NewShipType = StartShipConfig()"||smt == "NewSubSystemType = StartSubSystemConfig()" ||smt=="") {
				return new Literal(smt);
			}
			throw new ArgumentException("Unknown statement type: "+smt);
		}
	}
}
