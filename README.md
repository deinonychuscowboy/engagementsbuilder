# EngagementsBuilder

C# build system for the Engagements mod for Homeworld Remastered (https://steamcommunity.com/sharedfiles/filedetails/?id=500022516).

This is a really rudimentary lua parser for the lua .ship files used in Homeworld.
It's able to apply rules to various ship files to replace particular values in the ship files.
It also subs out values and moves them into HWRM's extremely powerful but extremely underutilized gamerule system.